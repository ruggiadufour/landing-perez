import React from "react";

export default function SVG({ svg, alt = "", className = "" }) {
  return <img src={svg} alt={alt} className={className} />;
}
