import React from "react";
const Home = () => {
  return (
    <>
      <section className="home">
        <div>
          <h1>
            Su satisfacción es nuestro objetivo y deber,
            <br />
            <span className="text-white-1">
              a partir de nuestro expertise profesional.
            </span>
          </h1>
          <a href="#contact" className="contact-button">
            CONTACTANOS
          </a>
        </div>
      </section>
      <div className="footer-gradient"></div>
    </>
  );
};

export default Home;
