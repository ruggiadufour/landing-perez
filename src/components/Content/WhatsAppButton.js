import React from "react";
import SVG from "../Globals/SVG";
import WhatsAppIcon from "../../assets/svgs/whatsapp-button-icon.svg";

export default function WhatsAppButton() {
  return (
    <a
      href="https://api.whatsapp.com/send?phone=+549116 591 9185"
      className="whatsapp-button"
      target="_blank"
      rel="noreferrer"
    >
      <SVG svg={WhatsAppIcon} alt="whatsapp" />
    </a>
  );
}
